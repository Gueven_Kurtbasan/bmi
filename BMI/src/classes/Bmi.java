package classes;

import java.util.*;;

public class Bmi {
	
	float groesse;
	float gewicht;
	
	public double calculate() {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Bitte Gr��e angeben: ");
		groesse = sc.nextFloat();
		System.out.print("Bitte Gewicht angeben: ");
		gewicht = sc.nextFloat();
		sc.close();
		double bmi=gewicht/Math.pow(groesse, 2);
		return bmi;
	}
}
